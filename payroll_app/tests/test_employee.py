import pytest
from employee import Employee


def test_employee_type_error():
    match = "Can't instantiate abstract class Employee with abstract method calculate_payroll"
    with pytest.raises(expected_exception=TypeError, match=match):
        Employee(100,'name')