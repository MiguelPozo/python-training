import pytest
from payroll import PayrollSystem
from employee import *


def test_calculate_payroll_none_list():
    payroll_system = PayrollSystem()
    result = payroll_system.calculate_payroll(None)
    assert result is not None


def test_calculate_payroll_empty_list():
    payroll_system = PayrollSystem()
    result = payroll_system.calculate_payroll([])
    assert result == ()


def test_calculate_payroll_non_empty_list():
    salary_employee = AdmistrativeWorker(1, 'John Doe', 1500)
    hourly_employee = ManufacturingWorker(2, 'Jane Doe', 40, 15)
    commission_employee = SalesAssociate(3, 'Foo Fighter', 1000, 250)
    payroll_system = PayrollSystem()
    result = payroll_system.calculate_payroll([
        salary_employee,
        hourly_employee,
        commission_employee
    ])
    assert result[0][1] == 1500
    assert result[1][1] == 600
    assert result[2][1] == 1250
