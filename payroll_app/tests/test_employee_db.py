import pytest
from employee_db import EmployeeDatabase
from employee import *


def test_employee_db_get_employees_not_none():
    db_employee = EmployeeDatabase()
    results = db_employee.get_employees()
    assert results is not None


def test_employee_db_get_employees_empty_list():
    db_employee = EmployeeDatabase()
    results = db_employee.get_employees()
    assert results is not []

    
def test_employee_db_get_employees():
    db_employee = EmployeeDatabase()
    results = db_employee.get_employees()
    for result in results:
        assert type(result) in [AdmistrativeWorker, SalesAssociate, ManufacturingWorker]



