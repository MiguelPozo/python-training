from src.duration import Duration


class Summary:
    
    
    def __init__(self, raw):
        self.title = raw['Title']
        self.id = raw['Id']
        self.attended_participants = raw['Attended participants']
        self.start_time = raw['Start Time']
        self.end_time = raw['End Time']
        duration1 = Duration(raw['Duration']['hours'], raw['Duration']['minutes'], raw['Duration']['seconds'])
        self.duration = duration1


    def __str__(self) -> str:
        return f"{{'Title': {self.title}, 'Id': {self.id}, 'Attended participants':{self.attended_participants}, 'Start Time': {self.start_time}, 'End Time': {self.end_time}, 'Duration': {str(self.duration)}}}"