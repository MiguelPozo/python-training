from src.attendance import Atendance
from src.summary_builder import build_summary_object
from src.participant_builder import build_participant_object

def build_attendance_object(raw_attendance):
    start_datetime = raw_attendance['Start Time']
    participants = build_list_participants(raw_attendance['Participants'])
    summary = build_summary_object(raw_attendance)
    attendance1 = Atendance(start_datetime, summary, participants)
    return attendance1

def build_list_participants(list_participants: list):
    participants = []
    for l in list_participants:
        participants.append(build_participant_object(l))
    return participants