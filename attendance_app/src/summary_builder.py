from src.summary import Summary
from src.duration import Duration
from datetime import datetime

summary_dict ={
    'Title': 'Title',
    'Id': 'Id',
    'Attended participants': 'Attended participants',
    'Start Time': 'Start Time',
    'End Time': 'End Time',
    'Duration': 'Duration',
    'Meeting title': 'Title',
    'Meeting Title': 'Title',
    'Start time': 'Start Time',
    'Meeting Start Time': 'Start Time',
    'End time': 'End Time',
    'Meeting End Time': 'End Time',
    'Meeting duration': 'Duration',
    'Meeting Id': 'Id',
    'Total Number of Participants': 'Attended participants',
}

def get_date(str_date: str):
    format_date = "%m/%d/%y, %I:%M:%S %p"
    return datetime.strptime(str_date, format_date)


def get_duration(start_time: datetime, end_time: datetime):
    duration = end_time - start_time
    duration = str(duration)
    return tuple(map(int, duration.split(":")))


def update_keys(raw: dict):
    new_dict = {}
    for key, value in raw.items():
        if key not in summary_dict: 
            summary_dict[key] = key
        new_dict[summary_dict[key]] = value
    return new_dict


def normalize_summary(raw_data: dict):
    new_raw = update_keys(raw_data)
    if 'Id' not in new_raw:
        new_raw['Id'] = new_raw['Title']
    start_time = get_date(new_raw['Start Time'])
    end_time = get_date(new_raw['End Time'])
    hours, minutes, seconds = get_duration(start_time, end_time)
    return {
            'Title': new_raw['Title'],
            'Id': new_raw['Id'],
            'Attended participants': int(new_raw['Attended participants']),
            'Start Time': new_raw['Start Time'],
            'End Time': new_raw['End Time'],
            'Duration': {
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds,
            }
        }


def build_summary_object(raw_data: dict):
    return Summary(normalize_summary(raw_data))
