from src.duration_helper import build_duration_object
from src.helpers import get_name_detailed


class Participant:
    
    def __init__(self, raw: dict):
        name, middle_name, first_surname, second_surname = get_name_detailed(raw)
        self.name = name
        self.middle_name = middle_name
        self.first_surname = first_surname
        self.second_surname = second_surname
        self.full_name = raw['Full Name']
        self.join_time = raw['Join time']
        self.leave_time = raw['Leave time']	
        # self.duration = build_duration_object(raw['Duration'])
        self.in_meeting_duration = build_duration_object(raw['In-meeting Duration'])
        self.email = raw['Email']
        self.role = raw['Role']
        self.id = raw['Participant ID (UPN)']
    
    def __str__(self) -> str:
        return f"{{'Full Name': {self.full_name}, 'Join time': {self.join_time},'Leave time': {self.leave_time}, 'Duration': {{'Hours': {self.in_meeting_duration.hours}, 'Minutes': {self.in_meeting_duration.minutes}, 'Seconds': {self.in_meeting_duration.seconds}}}, 'Email': {self.email}, 'Role': {self.role}, 'Participant ID (UPN)': {self.id}}}"
