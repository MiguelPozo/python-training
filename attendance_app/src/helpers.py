from os import listdir
from os.path import isfile, join
import csv
from datetime import datetime


def discover_data_files(path='attendance_app/data', pattern='csv'):
    return [join(path, f) for f in listdir(path) if isfile(join(path, f)) and f.endswith('csv')]


def read_file(files):
    readers = []
    for  f in files:
        csv_file = open(f, encoding='UTF-16')
        readers.append((f, csv.reader(csv_file, delimiter='\t')))
    return readers


def update_keys(raw: dict, map_table):
    new_dict = {}
    for key, value in raw.items():
        if key not in map_table: 
            map_table[key] = key
        new_dict[map_table[key]] = value
    return new_dict


def get_name_and_sur_name(email):
        full_name = email.split("@")[0]
        name = full_name.split('.')[0]
        first_surname = full_name.split('.')[1]
        return (name, first_surname)


def get_name_detailed(raw):
    name, first_surname = get_name_and_sur_name(raw['Email'])
    full_name = raw['Full Name'].split(' ')
    print(full_name)
    middle_name = None
    second_surname = None
    if len(full_name) > 2 :
        if first_surname == full_name[-1]:
            middle_name = full_name[-2]
        if first_surname == full_name[-2]:
            second_surname = full_name[-1]
        if first_surname != full_name[1]:
            middle_name = full_name[1]
    return (name, middle_name, first_surname, second_surname)


def get_date(str_date: str):
    format_date = "%m/%d/%y, %I:%M:%S %p"
    return datetime.strptime(str_date, format_date)


def get_duration(start_time: datetime, end_time: datetime):
    duration = end_time - start_time
    duration = str(duration)
    return tuple(map(int, duration.split(":")))