from src.participant import Participant
from src.helpers import update_keys, get_date, get_duration
import re
from datetime import timedelta

participant_map_table = {
        'Full Name': 'Full Name',
        'Join time': 'Join time',
        'Leave time': 'Leave time',	
        'In-meeting Duration':'In-meeting Duration',	
        'Email': 'Email',
        'Role': 'Role',
        'Participant ID (UPN)': 'Participant ID (UPN)',
        'Duration':'In-meeting Duration',
        'Name': 'Full Name',
        'First Join Time': 'Join time',
        'Last Leave time': 'Leave time',
        'Join Time': 'Join time',
        'Leave Time': 'Leave time',	
        }


def get_duration_str(time_duration: str):
    hms_letters = re.findall(r'[hms]', time_duration)
    hms_numbers = re.findall(r'\d*[^hms ]',
    time_duration)
    duration = dict(zip(hms_letters, hms_numbers))
    if 'h' not in duration.keys():
        duration['h'] = '0'
    if 'm' not in duration.keys():
        duration['m'] = '0'
    if 's' not in duration.keys():
        duration['s'] = '0'
    return {
        'Hours': int(duration['h']),
        'Minutes': int(duration['m']),
        'Seconds': int(duration['s']),	
    }
    
 
def normalize_participant(raw: dict):
    new_raw = update_keys(raw, participant_map_table)
    if isinstance(new_raw['In-meeting Duration'], str):
        duration = get_duration_str(new_raw['In-meeting Duration'])
    if isinstance(new_raw['In-meeting Duration'], dict):
        duration = new_raw['In-meeting Duration']
    if 'Participant ID (UPN)' not in new_raw.keys():
       new_raw['Participant ID (UPN)'] = new_raw['Email']
    return {
           'Full Name': new_raw['Full Name'],
           'Join time': new_raw['Join time'],
           'Leave time': new_raw['Leave time'],	
           'In-meeting Duration':duration,	
            'Email': new_raw['Email'],
            'Role': new_raw['Role'],
            'Participant ID (UPN)': new_raw['Participant ID (UPN)'],
        }

def resolve_participant_join_last_time(raw: dict):
    sum_duration = timedelta(seconds=0)
    min_date = get_date(raw[0]['Join Time'])
    join_time = raw[0]['Join Time']
    max_date = get_date(raw[0]['Leave Time'])
    leave_time = raw[0]['Leave Time']
    for r in raw:
        initial_date = get_date(r['Join Time'])
        if initial_date < min_date:
            min_date = initial_date
            join_time = r['Join Time']
        final_date = get_date(r['Leave Time'])
        if final_date > max_date:
           max_date = final_date
           leave_time = r['Leave Time']
        duration_tuple = get_duration(initial_date, final_date)
        duration = timedelta(hours=duration_tuple[0], minutes=duration_tuple[1], seconds=duration_tuple[2],)
        sum_duration = sum_duration + duration   
    duration_str = str(sum_duration).split(':')
    duration = f'{duration_str[0]}h {duration_str[1]}m {duration_str[2]}s'
    return {
        'Name': raw[0]['Full Name'],
        'First Join Time': join_time,
        'Last Leave time': leave_time,	
        'In-meeting Duration': duration,	
        'Email': raw[0]['Email'],
        'Role': raw[0]['Role'],
        'Participant ID (UPN)': raw[0]['Participant ID (UPN)']
    }

def build_participant_object(raw: dict):
    if raw ['Full Name'] is None:
        raise ValueError("Full Name, Join time, Leave time cannot be None")
    if raw ['Join time'] is None:
        raise ValueError("Full Name, Join time, Leave time cannot be None")
    if raw ['Leave time'] is None:
       raise ValueError("Full Name, Join time, Leave time cannot be None")
    return Participant(normalize_participant(raw))